package ideagen.test;

import java.util.List;

/**
 * Calculator to evaluate standard arithmetic expression. 
 */
public class Calculator {
	
	/** Tokenized arithmetic expression */
	private final List<Token> exprTokens;

	private Calculator(String exprStr) {
		if (exprStr == null) {
			throw new IllegalArgumentException("Input argument of arithmetic expression to evaluate cannot be null");
		} else if (exprStr.trim().length() == 0) {
			throw new IllegalArgumentException("Input argument of arithmetic expression to evaluate cannot be blank");
		}
		this.exprTokens = ArithmeticExpressionTokenizer.tokenize(exprStr);
	}

	/**
	 * Evaluate a given string of arithmetic expression and return its arithmetic result. 
	 */
	public static double calculate(String sum) {
		Calculator calc = new Calculator(sum);
		return calc.evaluateExprFragment(0).getResult();
	}
	
	/**
	 * Evaluate a fragment of arithmetic expression with operators.
	 * 
	 * @param startIndex Index of the first token to evaluate in source expression
	 * @return Evaluation result
	 * @throws IllegalArgumentException Illegal input in source expression is detected
	 */
	private EvaluationResult evaluateExprFragment(int startIndex) throws IllegalArgumentException {

		if (startIndex >= exprTokens.size()) {
			Token lastToken = exprTokens.get(exprTokens.size() - 1);
			throw new IllegalArgumentException("Unexpected end of expression after " + lastToken);
		}
		double leftOperand, rightOperand;
		int curOpPrecendence = 1;	// Set initial precedence as lowest 1
		
		// First starting left operand is mandatory, read it first
		EvaluationResult leftOperandEval = evaluateOperand(startIndex, curOpPrecendence);
		leftOperand = leftOperandEval.getResult();
		int curIndex = startIndex + leftOperandEval.getNumTokensEvaluated();

		// End of expression fragment will be at closing bracket, or end of input  
		while (curIndex < exprTokens.size() && !(exprTokens.get(curIndex) instanceof RightBracketToken)) {
			// Read one subsequent operator and right operand, then apply arithmetic operation
			OperatorToken operator = parseOperatorToken(curIndex);
			curOpPrecendence = operator.getPrecendence();
			int rightOperandIndex = curIndex + 1;
			EvaluationResult rightOperandEval = evaluateOperand(rightOperandIndex, curOpPrecendence);
			
			// Look ahead the following token after right operand to decide whether 
			// higher-precedence operation needs to be performed first on the right part 
			int nextTokenIndex = rightOperandIndex + rightOperandEval.getNumTokensEvaluated();
			if (nextTokenIndex < exprTokens.size()) {
				Token tokenAfterRightOperand = exprTokens.get(nextTokenIndex); 
				if (tokenAfterRightOperand instanceof OperatorToken && ((OperatorToken)tokenAfterRightOperand).getPrecendence() > curOpPrecendence) {
					// Evaluate operation on right part first before applying current arithmetic operation
					rightOperandEval = evaluateExprFragment(rightOperandIndex);
				}
			}
			rightOperand = rightOperandEval.getResult();
			leftOperand = compute(leftOperand, operator, rightOperand);
			
			// Proceed to inspect next set of operator & operand
			curIndex = curIndex + 1 + rightOperandEval.getNumTokensEvaluated();
		}
		return new EvaluationResult(leftOperand, curIndex - startIndex);
		
	}

	/**
	 * Evaluate stand alone operand token or a properly enclosed set of tokens which represents an operand.
	 * 
	 * @param index Index of the starting token in source expression to evaluate 
	 * @param curOpPrecedence Precedence level of current arithmetic operation in context.
	 * @return Evaluation result which represents numeric value of the evaluated operand.
	 * @throws IllegalArgumentException Illegal input in source expression is detected
	 */
	private EvaluationResult evaluateOperand(int index, int curOpPrecedence) throws IllegalArgumentException {
		if (index >= exprTokens.size()) {
			Token lastToken = exprTokens.get(exprTokens.size() - 1);
			throw new IllegalArgumentException("Unexpected end of expression after " + lastToken);
		}
		Token curToken = exprTokens.get(index);
		
		if (curToken instanceof NumberToken) {
			// It is stand alone numeric token
			double value = ((NumberToken) curToken).getValue();
			return new EvaluationResult(value, 1);
		} else if (curToken instanceof BracketToken) {
			// Expect a set of tokens enclosed by brackets
			if (curToken instanceof LeftBracketToken) {
				EvaluationResult innerEval = evaluateExprFragment(index + 1);
				
				// Validate whether there is closing bracket
				int endingTokenIndex = index + 1 + innerEval.getNumTokensEvaluated();
				if (endingTokenIndex < exprTokens.size()) {
					Token endingToken = exprTokens.get(endingTokenIndex);
					if (!(endingToken instanceof RightBracketToken)) {
						throw new IllegalArgumentException("Expected right bracket but got " + endingToken);
					} else {
						// No error, return result
						return new EvaluationResult(innerEval.getResult(), innerEval.getNumTokensEvaluated() + 2);
					}
				} else {
					Token lastToken = exprTokens.get(exprTokens.size() - 1);
					throw new IllegalArgumentException("Unexpected end of expression while expecting right bracket after " + lastToken);
				}

			} else {
				throw new IllegalArgumentException("Expected left bracket instead of " + curToken);
			}
		} else {
			throw new IllegalArgumentException("Expected a number or left bracket but got " + curToken);
		}
	}
	
	/**
	 * Attempt to extract an operator from token with given index.
	 */
	private OperatorToken parseOperatorToken(int index) {
		if (index >= exprTokens.size()) {
			Token lastToken = exprTokens.get(exprTokens.size() - 1);
			throw new IllegalArgumentException("Unexpected end of expression while expecting an operator after " + lastToken);
		}
		Token token = exprTokens.get(index);
		if (token instanceof OperatorToken) {
			return (OperatorToken) token;
		} else {
			throw new IllegalArgumentException("Expected an operator but got " + token);
		}
	}

	/**
	 * Perform standard binary arithmetic operation on two operands.
	 */
	private static double compute(double a, OperatorToken op, double b) {
		switch (op.getType()) {
		case ADDITION:
			return a + b;
		case SUBTRACTION:
			return a - b;
		case MULTIPLICATION:
			return a * b;
		case DIVISION:
			return a / b;
		default:
			throw new IllegalArgumentException("Unknown operator: " + op);
		}
	}

	/**
	 * Main entry method to test some input.
	 */
	public static void main(String[] args) {
		String[] exprs = {
			"1 + 1",
			"2 * 2",
			"1 + 2 + 3",
			"6 / 2",
			"11 + 23",
			"11.1 + 23",
			"1 + 1 * 3",
			"( 11.5 + 15.4 ) + 10.1",
			"23 - ( 29.3 - 12.5 )",
			"10 - ( 2 + 3 * ( 7 - 5 ) )",
			"1",
			"( ( 1 + 1 ) )",
			"( ( 1 + 2 + 3 ) * 2 / 2 ) + 2 + 2 * 22.2",
			") 1 + 2 (",
			"( 1 + 2 * 3",
			"+ 2",
			"1 /",
			" ",
			"1 * /",
			"1 + b"
		};
		
		for (String expr : exprs) {
			System.out.print(expr + " ==> ");
			try {
				System.out.print(calculate(expr));
			} catch (Exception e) {
				System.out.print("Error: " + e.getMessage());
			}
			System.out.println();
		}
		return;
	}
}
