package ideagen.test;

/**
 * A bracket token.
 */
public abstract class BracketToken extends Token {

	BracketToken(int location) {
		super(location);
	}
	
}
