package ideagen.test;

/**
 * A generic token.
 */
public abstract class Token {

	private final int location;

	Token(int location) {
		if (location <= 0) {
			throw new IllegalArgumentException("Location of token within source string must be greater than zero");
		}
		this.location = location;
	}

	public int getLocation() {
		return location;
	}
	
}
