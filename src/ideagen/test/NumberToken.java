package ideagen.test;

/**
 * A numeric token.
 */
public class NumberToken extends Token {

	private final double value;

	public NumberToken(int location, double value) {
		super(location);
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "number " + value + " at character " + getLocation();
	}
	
}
