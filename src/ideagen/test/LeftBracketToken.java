package ideagen.test;

/**
 * Left (opening) bracket. 
 */
public class LeftBracketToken extends BracketToken {

	public LeftBracketToken(int location) {
		super(location);
	}

	@Override
	public String toString() {
		return "left bracket at character " + getLocation();
	}

}
