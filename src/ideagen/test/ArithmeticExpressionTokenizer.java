package ideagen.test;

import ideagen.test.OperatorToken.OperatorType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArithmeticExpressionTokenizer {
	
	private static final Pattern SEPARATOR_PATTERN = Pattern.compile("[ ]+");
	
	public static List<Token> tokenize(String expr) {
		if (expr == null) {
			throw new IllegalArgumentException("Input argument of expression to tokenize cannot be null");
		}
		Matcher m = SEPARATOR_PATTERN.matcher(expr); 
		List<Token> tokens = new ArrayList<Token>();
		int curIndex = 0;

		while (m.find()) {
			int separatorIndex = m.start();
			if (separatorIndex > curIndex) {
				tokens.add(parseToken(curIndex, expr.substring(curIndex, separatorIndex)));
			}
			curIndex = m.end();
		}
		if (curIndex < expr.length()) {
			// Last token to extract
			tokens.add(parseToken(curIndex, expr.substring(curIndex, expr.length())));
		}
		return tokens;
	}
	
	private static Token parseToken(int index, String str) throws IllegalArgumentException {
		int location = index + 1;
		if (str.equals("+")) {
			return new OperatorToken(location, OperatorType.ADDITION);
		} else if (str.equals("-")) {
			return new OperatorToken(location, OperatorType.SUBTRACTION);
		} else if (str.equals("*")) {
			return new OperatorToken(location, OperatorType.MULTIPLICATION);
		} else if (str.equals("/")) {
			return new OperatorToken(location, OperatorType.DIVISION);
		} else if (str.equals("(")) {
			return new LeftBracketToken(location);
		} else if (str.equals(")")) {
			return new RightBracketToken(location);
		} else {
			try {
				return new NumberToken(location, Double.parseDouble(str));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Unrecognized token of \"" + str + "\"");
			}
		}
	}
	
}
