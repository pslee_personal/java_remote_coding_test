package ideagen.test;

/**
 * A right (closing) bracket).
 */
public class RightBracketToken extends BracketToken {

	public RightBracketToken(int location) {
		super(location);
	}

	@Override
	public String toString() {
		return "right bracket at character " + getLocation();
	}
	
}
