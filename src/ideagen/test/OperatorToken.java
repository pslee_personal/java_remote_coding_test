package ideagen.test;

/**
 * An operator token.
 */
public class OperatorToken extends Token {

	enum OperatorType {
		ADDITION,
		SUBTRACTION,
		MULTIPLICATION,
		DIVISION
	}
	
	private final OperatorType type;

	public OperatorToken(int location, OperatorType type) {

		super(location);
		this.type = type;
		
	}

	public OperatorType getType() {
		return type;
	}

	public int getPrecendence() {
		switch (type) {
		case ADDITION:
			return 1;
			
		case SUBTRACTION:
			return 1;

		case MULTIPLICATION:
			return 2;

		case DIVISION:
			return 2;
		
		default:
			throw new IllegalStateException("Unrecognized operator type: " + type);
		}
	}

	@Override
	public String toString() {
		return type.toString() + " operator at character " + getLocation();
	}
	
}
