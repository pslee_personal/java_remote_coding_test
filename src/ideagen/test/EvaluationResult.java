package ideagen.test;

/**
 * Result after an arithmetic evaluation.
 */
class EvaluationResult {

	private final double resultingValue;
	private final int numTokensEvaluated;

	public EvaluationResult(double resultingValue, int numTokensEvaluated) {
		this.resultingValue = resultingValue;
		this.numTokensEvaluated = numTokensEvaluated;
	}

	public double getResult() {
		return resultingValue;
	}

	public int getNumTokensEvaluated() {
		return numTokensEvaluated;
	}
	
}